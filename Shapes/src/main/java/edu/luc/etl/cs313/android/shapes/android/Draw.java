package edu.luc.etl.cs313.android.shapes.android;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;

import java.util.List;

import edu.luc.etl.cs313.android.shapes.model.*;

/**
 * A Visitor for drawing a shape to an Android canvas.
 */
public class Draw implements Visitor<Void> {

	private final Canvas canvas;

	private final Paint paint;

	public Draw(final Canvas canvas, final Paint paint) {
		this.canvas = canvas;
		this.paint = paint;
		paint.setStyle(Style.STROKE);
	}

	@Override
	public Void onCircle(final Circle c) {
		canvas.drawCircle(0, 0, c.getRadius(), paint);
		return null;
	}

	@Override
	public Void onStroke(final Stroke c) {
		int oldColor = paint.getColor();
		Style oldStyle = paint.getStyle();
		paint.setColor(c.getColor());
		paint.setStyle(Style.FILL_AND_STROKE);
		c.getShape().accept(this);
		paint.setColor(oldColor);
		paint.setStyle(oldStyle);
		return null;
	}

	@Override
	public Void onFill(final Fill f) {
		Style oldStyle = paint.getStyle();
		paint.setStyle(Style.FILL);
		f.getShape().accept(this);
		paint.setStyle(oldStyle);
		return null;
	}

	@Override
	public Void onGroup(final Group g) {
		List<? extends Shape> shapes = g.getShapes();
		for(int i=0; i<shapes.size(); i++) {
			Shape l = shapes.get(i);
			l.accept(this);
		}
		return null;
	}

	@Override
	public Void onLocation(final Location l) {
		canvas.translate(l.getX(), l.getY());
		l.getShape().accept(this);
		canvas.translate(-l.getX(), -l.getY());
		return null;
	}

	@Override
	public Void onRectangle(final Rectangle r) {
		canvas.drawRect(0, 0, r.getWidth(), r.getHeight(), paint);
		return null;
	}

	@Override
	public Void onOutline(Outline o) {
		Style oldStyle = paint.getStyle();
		paint.setStyle(Style.STROKE);
		o.getShape().accept(this);
		paint.setStyle(oldStyle);
		return null;
	}

	@Override
	public Void onPolygon(final Polygon s) {
		float[] pts = new float[s.getPoints().size()*4];
		List<? extends Point> points = s.getPoints();
		int tempArraySize = 0;
		pts[tempArraySize] = points.get(0).getX();
		pts[tempArraySize+1] = points.get(0).getY();
		tempArraySize = tempArraySize + 2;

		for (int i=1; i<s.getPoints().size(); i++) {
			pts[tempArraySize] = points.get(i).getX();
			pts[tempArraySize+1] = points.get(i).getY();
			pts[tempArraySize+2]=pts[tempArraySize];
			pts[tempArraySize+3]=pts[tempArraySize+1];
			tempArraySize = tempArraySize + 4;
		}

		pts[tempArraySize] = points.get(0).getX();
		pts[tempArraySize + 1] = points.get(0).getY();

		canvas.drawLines(pts, paint);

		return null;
	}
}
