package edu.luc.etl.cs313.android.shapes.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

/**
 * the bounding box, that is, the smallest
 * rectangle containing the shape. The resulting bounding box is returned as a
 * rectangle at a specific location.
 */
public class BoundingBox implements Visitor<Location> {


	@Override
	public Location onCircle(final Circle c) {
		final int radius = c.getRadius();
		return new Location(-radius, -radius, new Rectangle(2 * radius, 2 * radius));
	}



	@Override
	public Location onFill(final Fill f) {
		return f.getShape().accept(this);
	}

	@Override
	public Location onGroup(final Group g) {
		List<? extends Shape> shapes = g.getShapes();

		List<Integer> ptsMinX = new ArrayList<Integer>();
		List<Integer> ptsMinY = new ArrayList<Integer>();
		List<Integer> ptsMaxX = new ArrayList<Integer>();
		List<Integer> ptsMaxY = new ArrayList<Integer>();

		for (int i=0; i<shapes.size(); i++) {
			final Location l = shapes.get(i).accept(this);
			Rectangle r = (Rectangle) l.getShape();
			ptsMinX.add(l.getX());
			ptsMinY.add(l.getY());
			ptsMaxX.add(l.getX() + r.getWidth());
			ptsMaxY.add(l.getY() + r.getHeight());
		}

		Collections.sort(ptsMinX);
		Collections.sort(ptsMaxX);
		Collections.sort(ptsMinY);
		Collections.sort(ptsMaxY);

		int minX = ptsMinX.get(0);
		int maxX = ptsMaxX.get(ptsMaxX.size() - 1);

		int minY = ptsMinY.get(0);
		int maxY = ptsMaxY.get(ptsMaxY.size() - 1);

		return new Location(minX, minY, new Rectangle(maxX-minX, maxY-minY));
	}

	@Override
	public Location onLocation(final Location l) {
		final Location shape = l.getShape().accept(this);
		return new Location (l.getX()+shape.getX(), l.getY()+shape.getX(), shape.getShape());
	}

	@Override
	public Location onRectangle(final Rectangle r) {
		final int width = r.getWidth();
		final int height = r.getHeight();
		return new Location(0, 0, new Rectangle(width, height));	}

	@Override
	public Location onStroke(final Stroke c) {
		return c.getShape().accept(this);
	}

	@Override
	public Location onOutline(final Outline o) {
		return o.getShape().accept(this);
	}

	@Override
	public Location onPolygon(final Polygon s) {
		List<? extends Point> points = s.getPoints();
		List<Integer> xPts = new ArrayList<Integer>();
		List<Integer> yPts = new ArrayList<Integer>();

		for(int i=0; i<points.size();i++){
			xPts.add(points.get(i).getX());
			yPts.add(points.get(i).getY());
		}

		Collections.sort(xPts);
		Collections.sort(yPts);
		int xMin = xPts.get(0);
		int xMax = xPts.get(xPts.size()-1);
		int yMin = yPts.get(0);
		int yMax = yPts.get(yPts.size()-1);

		return new Location(xMin, yMin, new Rectangle(xMax-xMin, yMax-yMin));
	}
}
